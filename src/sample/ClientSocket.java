package sample;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientSocket {
    private InetAddress serverHost;
    private int serverPort;
    private Socket socket;

    private InputStream serverInput;
    private OutputStream serverOutput;

    private int BUFF_LEN = 100;

    public ClientSocket(String serverHost, int serverPort) throws IOException {
        this.serverHost = InetAddress.getByName(serverHost);
        this.serverPort = serverPort;
        this.socket = new Socket(this.serverHost, this.serverPort);

        this.serverInput = this.socket.getInputStream();
        this.serverOutput = this.socket.getOutputStream();
    }

    public  String getMessageFromServer() throws IOException {
        byte[] data = new byte[BUFF_LEN];
        int size = this.serverInput.read(data);
        return new String(data, 0, size,  "UTF-8");
    }

    public void sendMessageToServer(String message) throws IOException {
        byte[] b = message.getBytes("UTF-8");
        this.serverOutput.write(b);
        this.serverOutput.flush();
    }

    public void disconnect() throws IOException {
        this.serverInput.close();
        this.serverOutput.close();
        this.socket.close();
    }
}
