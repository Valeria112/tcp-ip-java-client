package sample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.*;

public class Main extends Application {

    private String APPLICATION_NAME = "Игра логика";

    @Override
    public void start(Stage primaryStage) throws Exception {

        int serverPort = 9090;
        String serverHost = "localhost";
        ClientSocket clientSocket = new ClientSocket(serverHost, serverPort);

        clientSocket.sendMessageToServer("start");
        String answer = clientSocket.getMessageFromServer();

        StartController startController = new StartController(primaryStage, clientSocket);

        if (answer.equals("name")) {
            primaryStage.setScene(new Scene(startController));
            primaryStage.setTitle(APPLICATION_NAME);
            primaryStage.show();
        }
    }

    public static void main(String[] args) throws IOException {
        launch(args);
    }
}
