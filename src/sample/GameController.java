package sample;
import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class GameController extends VBox {
    @FXML
    private Text textQuestion;
    @FXML
    private RadioButton radioButtonOne;
    @FXML
    private RadioButton radioButtonTwo;
    @FXML
    private RadioButton radioButtonThree;
    @FXML
    private Button buttonNext;

    private String selectedText;
    private ClientSocket client;
    private Stage primaryStage;

    public GameController(ClientSocket clientSocket, Stage stage) {
        this.client = clientSocket;
        this.primaryStage = stage;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("game_scene.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        buttonNext.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    nextButtonListener();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void setTextQuestion(String text) {
        this.textQuestion.setText(text);
    }

    public void setRadioButtonsNames(String[] radioButtonsNames) {
        assert radioButtonsNames.length == 3;

        this.radioButtonOne.setText(radioButtonsNames[0]);
        this.radioButtonTwo.setText(radioButtonsNames[1]);
        this.radioButtonThree.setText(radioButtonsNames[2]);
    }

    protected void nextButtonListener() throws IOException {
        if(radioButtonOne.isSelected())
            this.selectedText = radioButtonOne.getText();
        else if(radioButtonTwo.isSelected())
            this.selectedText = radioButtonTwo.getText();
        else if(radioButtonThree.isSelected())
            this.selectedText = radioButtonThree.getText();

        this.client.sendMessageToServer(getSelectedText());
        String answer = client.getMessageFromServer();

        if (answer.equals("wrong answer")) {
            stopGame();
        }
        else {
            String[] result = answer.split("\n");
            this.setRadioButtonsNames(result);
        }
    }

    private void stopGame() throws IOException {
        client.sendMessageToServer("result");
        String answer = client.getMessageFromServer();
        int intAnswer = Integer.parseInt(answer);

        StopController stopController = new StopController(client, primaryStage);
        stopController.setTextResult(intAnswer);
        this.primaryStage.setScene(new Scene(stopController));
    }

    public String getSelectedText() {
        return this.selectedText;
    }
}
