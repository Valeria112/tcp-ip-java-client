package sample;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;


public class StartController extends VBox {

    @FXML
    Button buttonStart;
    @FXML
    TextField textField;

    Stage primaryStage;
    ClientSocket clientSocket;

    public StartController(Stage primaryStage, ClientSocket clientSocket) {
        this.primaryStage = primaryStage;
        this.clientSocket = clientSocket;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("start_scene.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        buttonStart.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    gameStart();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public String getTextFromField() {
        return this.textField.getText();
    }

    protected void gameStart() throws IOException {
        this.clientSocket.sendMessageToServer(getTextFromField());
        GameController customControl = new GameController(this.clientSocket, this.primaryStage);

        String answer = clientSocket.getMessageFromServer();
        String[] result = answer.split("\n");

        customControl.setTextQuestion("Выберите лишнее слово:");
        customControl.setRadioButtonsNames(result);

        this.primaryStage.setScene(new Scene(customControl));
    }
}
