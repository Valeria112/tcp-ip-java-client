package sample;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

public class StopController extends VBox{

    @FXML
    private Text textResult;
    @FXML
    private Button buttonOut;
    @FXML
    private Button buttonShowBestResults;

    private ClientSocket client;
    private Stage primaryStage;

    public StopController(ClientSocket clientSocket, Stage stage) {
        this.client = clientSocket;
        this.primaryStage = stage;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("stop_scene.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        this.buttonOut.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    client.sendMessageToServer("stop");
                    client.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                primaryStage.close();
            }
        });

        this.buttonShowBestResults.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    client.sendMessageToServer("best_res");
                    setText(client.getMessageFromServer());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void setTextResult(int result) {
        String name = " очк";
        int rest = result % 10;

        if (rest == 0 || rest > 4 || (result > 10 && result < 21))
            name += "ов!";
        else if (rest == 1)
            name += "о!";
        else
            name += "а!";

        setText("Вы набрали "  + result + name);
    }

    public void setText(String result) {
        textResult.setText(result);
    }

}
